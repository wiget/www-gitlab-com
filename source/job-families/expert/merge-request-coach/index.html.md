---
layout: markdown_page
title: "Merge Request Coach"
---

The main goal of a Merge Request Coach is to help
[merge requests from the community](https://gitlab.com/gitlab-org/gitlab/merge_requests?label_name[]=Community%20contribution)
get merged into GitLab.

## Responsibilities

* Triage merge requests labeled `~Community Contribution`.
* Close merge requests that we don't want, with a clear explanation on the
  reasons why, so that people don't feel discouraged.
* Help contributors to get their merge requests to meet the
  [contribution acceptance criteria](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#contribution-acceptance-criteria).
* Read the weekly `Merge requests requiring attention` email to follow-up on inactive MRs. Each coach should pick a few MRs to follow-up each week. 
* Help find and assign merge requests to available reviewers.
* If the contributor is unresponsive or if they are unable to finish it, finish
  their merge requests. Also, see the [closing policy for issues and merge requests](https://docs.gitlab.com/ee/development/contributing/#closing-policy-for-issues-and-merge-requests).
  1. Close the original merge request and say that you will finish it.
  1. Check out the branch locally.
  1. Make sure a changelog entry crediting the author exists.
  1. Add your own commits to improve and finish the original work.
  1. Push to a new branch and open a new merge request.
* Make it easy to contribute to GitLab even for people who are new to Ruby,
  JavaScript, Golang, etc. or programming entirely. For example, you can add any hints or possible fixes on issues that are open for community contribution.  

More information on Merge Request Coach is available in the [handbook](/handbook/marketing/community-relations/code-contributor-program/resources/merge-request-coach-lifecycle.html). 
