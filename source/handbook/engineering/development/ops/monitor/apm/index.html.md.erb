---
layout: handbook-page-toc
title: "APM Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Common links

* Slack channel: [#g_monitor_apm](https://gitlab.slack.com/archives/g_monitor_apm)
* Slack alias: @monitor-apm-group
* Google groups: monitor-apm-group@gitlab.com (whole team), monitor-apm-be@gitlab.com (backend team), and monitor-apm-fe@gitlab.com (frontend team)

## Backend Team members

<%= direct_team(manager_role: 'Engineering Manager, Monitor:APM') %>

## Frontend Team members

<%= direct_team(manager_role: 'Interim Frontend Engineering Manager, Monitor:APM') %>

## Stable counterparts

<%= stable_counterparts(role_regexp: /[,&] Monitor/, direct_manager_role: 'Engineering Manager, Monitor:Health', other_manager_roles: ['Engineering Manager, Monitor:APM', 'Frontend Engineering Manager, Monitor:Health', 'Interim Frontend Engineering Manager, Monitor:APM']) %>

## Responsibilities
{: #monitoring}

The APM group is responsible for:
* Providing the tools required to enable monitoring of GitLab.com
* Packaging these tools to enable all customers to manage their instances easily and completely
* Building integrated monitoring solutions for customers apps into GitLab, including: metrics, logging, and tracing

This team maps to the [APM Group](/handbook/product/categories/#apm-group) category.

## How to work with APM

### Adding new metrics to GitLab

The APM Group is responsible for providing the underlying libraries and tools to enable GitLab team-members to instrument their code. When adding new metrics, we need to consider a few facets: the impact on GitLab.com, customer deployments, and whether any default alerting rules should be provided.

Recommended process for adding new metrics:

1. Open an issue in the desired project outlining the new metrics desired
1. Label with the ~group::apm label, and ping @gl-monitoring for initial review
1. During implementation consider:
   1. The Prometheus [naming](https://prometheus.io/docs/practices/naming/) and [instrumentation](https://prometheus.io/docs/practices/instrumentation/) guidelines
   1. Impact on cardinality and performance of Prometheus
   1. Whether any alerts should be created
1. Assign to an available APM Group reviewer

### Milestone Planning

In planning a milestone, the engineering and product team work closely together. We do follow the [Engineering Workflow](/handbook/engineering/workflow/) and specifically the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) when planning a milestone. However, here is some additional information that may help in the planning process.

For APM we use the [APM Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1065731) to do milestone planning and to prioritize our backlog. We defer to the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) for deadlines. The following information outlines how we execute those tasks in the context of our issue board where `M` is the month in which milestone `m` will be shipped:

1. By the `4th` of `M-1` (at least 14 days before milestone `m` begins):
    * Organize the upcoming release column by priority. This serves as a draft of issues to be included in `m`.

1. Organize the upcoming release column by priority.
    * Due Date: First day of the month. *Note*: this aligns with the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline)
    
        | Responsible | Accountable | Consulted |
        | ----------- | ----------- | --------- |
        | PM          | PM          | EMs       |

    * Identify any issues that will not be completed in `m-1` and move them to `m`. Apply the "to schedule" label for any that are moved, and apply a weight (see [Issue Weights](#issue-weights)). __Note__: This may continue to happen leading up to our deadline for finalized scope on the `13th` of `M-1`

        | Responsible | Accountable | Consulted |
        | ----------- | ----------- | --------- |
        | EMs         | PM          | Engineers |

1. By the `13th` of `M-1` (at least 5 days before milestone `m` begins):
    * Scope should be finalized with the [`deliverable`](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#release-scoping-labels) label applied to all issues scheduled for `m`.

        | Responsible | Accountable | Consulted |
        | ----------- | ----------- | --------- |
        | EMs         | PM          | Engineers |

    * Apply the `release post item` label to the top issues for the upcoming milestone that we want to highlight in the Kickoff call.

        | Responsible | Accountable | Informed |
        | ----------- | ----------- | -------- |
        | PM          | PM          | EMs      |

1. After the `22nd` of `M-1` (release date for `m-1`):
    * Ensure all remaining issues in `m-1` have either been moved to `m` or to a later milestone for reprioritization at a later time.

        | Responsible | Accountable | Informed |
        | ----------- | ----------- | -------- |
        | EMs and PM  | PM          | Assignees|

By using the planning board as a priority list, and by keeping it in order, then we should always be able to look at the current and upcoming milestone columns to have a prioritized list of upcoming work.

### Issue Weights

We only use issue weights when we have to move an issue from one milestone to the next. This is to help us understand how much remaining work we have for any issue that had to move. For example, we may schedule an issue that just needs a final review differently than an issue that has not been started. We use a simple 1 to 10 scale to estimate the remaining work:

| Weight | Meaning       |
| ------ | ------------- |
| 1      | 10% Remaining |
| 5      | 50% Remaining |
| 10     | 100% Remaining/Not Started |

## Recurring Meetings
While we try to keep our process pretty light on meetings, we do hold a [Monitor APM Weekly Meeting](https://docs.google.com/document/d/1Y9woIjy7ySV3lbIJHuoyROYPZhtO1L_w3XDhgGKzZt8/edit?usp=sharing) to triage and prioritize new issues, discuss our upcoming issues, and uncover any unknowns.

## Async Daily Standups
The purpose of our async standups is to allow every team member to have insight into what everyone else is doing and whether anyone is blocked and could use help. This should not be an exhaustive list of all of your tasks for the day, but rather a summary of the major deliverable you are hoping to achieve. All question prompts are optional. We use the [geekbot slack plugin](https://geekbot.io/) to automate our async standup in the [#g_monitor_standup_apm](https://gitlab.slack.com/messages/CNYUY47FW) channel. Every team member should be added to the async standup by their manager.

## Repos we own or use
* [Prometheus Ruby Mmap Client](https://gitlab.com/gitlab-org/prometheus-client-mmap) - The ruby Prometheus instrumentation lib we built, which we used to instrument GitLab
* [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce) and [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee) - Where much of the user facing code lives
* [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab) and [Charts](https://gitlab.com/charts/charts.gitlab.io), where a lot of the packaging related work goes on. (We ship GitLab fully instrumented along with a Prometheus instance)

## Issue boards

* [APM](https://gitlab.com/groups/gitlab-org/-/boards/1143499) - Main board with all issues labeled "group::apm"
* [APM - Planning](https://gitlab.com/groups/gitlab-org/-/boards/1065731) - APM issues organized by milestone
* [APM - Product](https://gitlab.com/groups/gitlab-org/-/boards/1117038) - APM issues organized by issue type label like "feature", "bug", "backstage", "security", or "Community contribution"
* [APM - Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1165027) - APM issues organized by workflow label of "ready for development", "in dev", or "in review".
